//
// Created by Federico Fuga on 11/07/18.
//

#include "GeoGraphBuilder.h"
#include "GeoGraph.h"

GeoGraphBuilder::GeoGraphBuilder(size_t numVert, size_t numEdges)
        : numVert(numVert), numEdges(numEdges)
{
}

void GeoGraphBuilder::build(GeoGraph &graph)
{
    std::mt19937 randomGenerator = std::mt19937(time(0));
    std::uniform_real_distribution<float> un(-45, 45);

    for (size_t i = 0; i < numVert; ++i) {
        graph.addNode(i, un(randomGenerator), un(randomGenerator));
    }

    std::uniform_int_distribution<size_t> uni(0, numVert-1);
    for (size_t i = 0; i < numEdges; ++i) {
        auto st = uni(randomGenerator);
        auto to = uni(randomGenerator);

        float dx = graph.locations[st].x - graph.locations[to].x;
        float dy = graph.locations[st].y - graph.locations[to].y;

        graph.addEdge(st, to, sqrt(dx * dx + dy * dy));
    }
}
