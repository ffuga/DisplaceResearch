//
// Created by Federico Fuga on 11/07/18.
//

#ifndef EXPERIMENTS_GEOGRAPHBUILDER_H
#define EXPERIMENTS_GEOGRAPHBUILDER_H

#include <cstddef>

class GeoGraph;

class GeoGraphBuilder {
    size_t numVert, numEdges;
public:
    GeoGraphBuilder(size_t numVert, size_t numEdges);

    void build(GeoGraph &graph);
};


#endif //EXPERIMENTS_GEOGRAPHBUILDER_H
