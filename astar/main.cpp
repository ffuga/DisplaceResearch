/** @file 
 * @author Federico Fuga <fuga@studiofuga.com>
 * @date 11/07/18
 */

#include "GeoGraph.h"
#include "GeoGraphBuilder.h"
#include "GeoGraphLoader.h"
#include "AStarShortestPathFinder.h"

#include <chrono>

int main(int argc, char *argv[])
{
    GeoGraph graph;
    GeoGraphBuilder builder(1000, 5000);
    GeoGraphLoader loader;
    AStarShortestPathFinder finder;

    //builder.build(graph);
    loader.load(graph, "../../coord1.dat", "../../graph1.dat");

    std::cout << "Graph: " << graph.numNodes() << " nodes, " << graph.numEdges() << " edges\n";

    int numiterations = 1000;


    for (int iteration = 0; iteration < numiterations; ++iteration) {
        auto start_node = graph.selectRandomNode();
        auto end_node = graph.selectRandomNode();

        auto starttime = std::chrono::high_resolution_clock::now();
        auto path = finder.findShortestPath(graph, start_node, end_node);
        auto endtime = std::chrono::high_resolution_clock::now();
        auto diff = endtime - starttime;

        std::cout << "It: " << iteration
                  << " from: " << start_node << " to: " << end_node
                  << " path length: " << path.size()
                  << " time: " << std::chrono::duration_cast<std::chrono::microseconds>(diff).count() << "us\n";

        /*
        std::cout << "   > ";
        for (auto p : path) {
            std::cout << p << " ";
        }
        std::cout << "\n";*/
    }

    return 0;
}
